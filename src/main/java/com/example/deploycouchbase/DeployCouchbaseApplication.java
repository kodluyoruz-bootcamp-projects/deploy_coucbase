package com.example.deploycouchbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@Controller
public class DeployCouchbaseApplication {


    @GetMapping("/")
    public String home() {
        return "redirect:/swagger-ui.html";
    }

    public static void main(String[] args) {
        SpringApplication.run(DeployCouchbaseApplication.class, args);
    }

}
