package com.example.deploycouchbase.config;

import com.couchbase.client.core.env.TimeoutConfig;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.ClusterOptions;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.codec.JacksonJsonSerializer;
import com.couchbase.client.java.env.ClusterEnvironment;
import com.couchbase.client.java.json.JsonValueModule;
import com.couchbase.client.java.manager.bucket.BucketSettings;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.Duration;
import java.util.Map;

@Configuration
public class CouchBaseConfiguration {

    private final CouchBaseProperties couchbaseProperties;

    public CouchBaseConfiguration(CouchBaseProperties couchbaseProperties) {
        this.couchbaseProperties = couchbaseProperties;
    }

    @Bean
    public Cluster couchbaseCluster() {
        ObjectMapper mapper = new Jackson2ObjectMapperBuilder()
                .modules(new JsonValueModule())
                .serializationInclusion(JsonInclude.Include.NON_NULL)
                .build();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        ClusterEnvironment clusterEnvironment = ClusterEnvironment.builder()
                .jsonSerializer(JacksonJsonSerializer.create(mapper))
                .timeoutConfig(TimeoutConfig
                        .connectTimeout(Duration.ofSeconds(12))
                        .searchTimeout(Duration.ofSeconds(12))
                        .managementTimeout(Duration.ofSeconds(12))
                        .queryTimeout(Duration.ofSeconds(12)))
                .build();
        return Cluster.connect(couchbaseProperties.getHost(), ClusterOptions
                .clusterOptions(couchbaseProperties.getUserName(), couchbaseProperties.getPassword())
                .environment(clusterEnvironment));
    }

    @Bean
    public Collection authCollection(Cluster couchbaseCluster) {
        String bucketName = couchbaseProperties.getBucketNames().get(0);
        return collectionOperator(couchbaseCluster, bucketName);
    }
    
    private Collection collectionOperator(Cluster couchbaseCluster, String bucketName) {
        if (!bucketAlreadyExist(couchbaseCluster, bucketName))
            couchbaseCluster.buckets()
                    .createBucket(BucketSettings.create(bucketName));

        couchbaseCluster.bucket(bucketName).waitUntilReady(Duration.ofSeconds(122));
        return couchbaseCluster.bucket(bucketName).defaultCollection();
    }

    private boolean bucketAlreadyExist(Cluster couchbaseCluster, String bucketName) {
        Map<String, BucketSettings> allBuckets = couchbaseCluster.buckets().getAllBuckets();
        return allBuckets.containsKey(bucketName);
    }

}
