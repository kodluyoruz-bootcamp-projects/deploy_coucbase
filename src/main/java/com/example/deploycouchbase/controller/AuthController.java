package com.example.deploycouchbase.controller;


import com.example.deploycouchbase.model.User;
import com.example.deploycouchbase.service.CrudService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final CrudService crudService;

    public AuthController(CrudService crudService) {
        this.crudService = crudService;
    }

    @GetMapping
    public ResponseEntity<User> getUser(@RequestParam String id) {
        return ResponseEntity.ok(crudService.getUser(id));
    }

    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody User user) {
        crudService.createUser(user);
        return ResponseEntity.ok("ok..");
    }

    @DeleteMapping
    public ResponseEntity<String> deleteUser(@RequestParam String id) {
        crudService.delete(id);
        return ResponseEntity.ok("ok..");
    }

    @PutMapping
    public ResponseEntity<String> updateUser(@RequestBody User user) {
        crudService.update(user);
        return ResponseEntity.ok("ok..");

    }
}
