package com.example.deploycouchbase.repository;


import com.couchbase.client.java.Collection;
import lombok.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;



public abstract class Crud<T> implements ICrud<T> {
    protected final Collection collection;

    public Crud(Collection collection) {
        this.collection = collection;

    }

    @Override
    public void create(@NonNull String id, @NonNull T t) {
        collection.insert(id, t);
    }

    @Override
    public void updateById(@NonNull String id, @NonNull T t) {
        collection.replace(id, t);
    }
    @Override
    public void upsert(String id, T t) {
        collection.upsert(id,t);
    }

    @Override
    public void deleteById(@NonNull String id) {
        collection.remove(id);
    }

    @Override
    public boolean existsById(String id) {
        return collection.exists(id).exists();
    }


}
