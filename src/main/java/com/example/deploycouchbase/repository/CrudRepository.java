package com.example.deploycouchbase.repository;

import com.couchbase.client.java.Collection;
import com.example.deploycouchbase.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class CrudRepository extends Crud<User> {

    private final Collection collection;

    public CrudRepository(Collection collection) {
        super(collection);
        this.collection=collection;
    }

    @Override
    public User findById(String id) {
        return collection.get(id).contentAs(User.class);
    }
}
