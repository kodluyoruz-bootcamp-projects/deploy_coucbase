package com.example.deploycouchbase.service;

import com.example.deploycouchbase.model.User;
import com.example.deploycouchbase.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class CrudService {

    private final CrudRepository repository;

    public CrudService(CrudRepository repository) {
        this.repository = repository;
    }

    public void createUser(User user){
        repository.create(user.getId(),user);
    }

    public User getUser(String id){
        return repository.findById(id);
    }

    public void delete (String id){
        repository.deleteById(id);
    }

    public void update(User user){
        repository.upsert(user.getId(),user);
    }

}
